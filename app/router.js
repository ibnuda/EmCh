import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('boards', {path: '/'}, function() {
    this.route('board', {path: '/:board_id'}, function() {
      this.route('thread', {path: '/:thread_id'});
    });
  });
  this.route('not-found', {path: '/*wildcard'});
});

export default Router;
