/*
import DS from 'ember-data';

export default DS.Model.extend({
  no: DS.attr(),
  now: DS.attr(),
  name: DS.attr(),
  com: DS.attr(),
  filename: DS.attr(),
  w: DS.attr(),
  h: DS.attr(),
  tn_w: DS.attr(),
  tn_h: DS.attr(),
  tim: DS.attr(),
  time: DS.attr(),
  md5: DS.attr(),
  fsize: DS.attr(),
  resto: DS.attr(),
  bumplimit: DS.attr(),
  semantic_url: DS.attr(),
  replies: DS.attr(),
  images: DS.attr(),
  omitted_posts: DS.attr(),
  omitted_images: DS.attr(),
  last_replies: DS.hasMany('reply')
});
 */

import {Model, attr, hasMany} from 'ember-restless';

var Thread = Model.extend({
  no: attr(),
  now: attr(),
  name: attr(),
  com: attr(),
  filename: attr(),
  w: attr(),
  h: attr(),
  tn_w: attr(),
  tn_h: attr(),
  tim: attr(),
  time: attr(),
  md5: attr(),
  fsize: attr(),
  resto: attr(),
  bumplimit: attr(),
  semantic_url: attr(),
  replies: attr(),
  images: attr(),
  omitted_posts: attr(),
  omitted_images: attr(),
  last_replies: hasMany('reply')
});

Thread.reopenClass({
  resourceName: 'thread'
});

export default Thread;
