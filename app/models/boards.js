/*import DS from 'ember-data';

export default DS.Model.extend({
  page: DS.attr(),
  threads: DS.hasMany('thread')
});
*/

import {Model, attr} from 'ember-restless';

var Boards = Model.extend({
  board: attr(),
  title: attr(),
  ws_board: attr(),
  per_page: attr(),
  pages: attr(),
  max_filesize: attr(),
  max_webm_size: attr(),
  max_comment_chars: attr(),
  max_webm_duration: attr(),
  bump_limit: attr(),
  image_limit: attr(),
  meta_description: attr()
});

Boards.reopenClass({
  resourceName: 'boards'
});

export default Boards;
