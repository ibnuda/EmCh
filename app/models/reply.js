/*
import DS from 'ember-data';

export default DS.Model.extend({
  no: DS.attr(),
  now: DS.attr(),
  name: DS.attr(),
  com: DS.attr(),
  filename: DS.attr(),
  w: DS.attr(),
  h: DS.attr(),
  tn_w: DS.attr(),
  tn_h: DS.attr(),
  tim: DS.attr(),
  time: DS.attr(),
  md5: DS.attr(),
  fsize: DS.attr(),
  resto: DS.attr(),
  thread: DS.belongsTo('thread')
});
 */

import {Model, attr} from 'ember-restless';

var Reply = Model.extend({
  no: attr(),
  now: attr(),
  name: attr(),
  com: attr(),
  filename: attr(),
  w: attr(),
  h: attr(),
  tn_w: attr(),
  tn_h: attr(),
  tim: attr(),
  time: attr(),
  md5: attr(),
  fsize: attr(),
  resto: attr()
  // thread: DS.belongsTo('thread')
});

Reply.reopenClass({
  resourceName: 'reply'
});

export default Reply;
