import {Client} from 'ember-restless';

export function initialize(/* application */) {
  // application.inject('route', 'foo', 'service:foo');
  var application = arguments[1] || arguments[0];
  application.set('Client', Client.create());
}

export default {
  name: 'something',
  initialize
};
